class terraform::install {
  ensure_packages(['unzip'])

  if $terraform::version == 'latest' {
    $version = terraform_last_version()
  } else {
    $version = $terraform::version
  }

  $download_url = regsubst($terraform::download_url, '\$\{version\}', $version, 'G')

  archive { '/tmp/terraform.zip':
    ensure        => present,
    extract       => true,
    extract_path  => $terraform::bin_dir,
    source        => $download_url,
    creates       => "$terraform::bin_dir/terraform",
    cleanup       => true,
  }
}
